const limitFunctionCallCount = require("../limitFunctionCallCount"); 

const limitedFunction = limitFunctionCallCount(function(){
    return (`Function called!`);
},2)

console.log(limitedFunction());
console.log(limitedFunction());
console.log(limitedFunction());
