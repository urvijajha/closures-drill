function cacheFunction(cb) {
 try {
    const cache = {};

    function execute(num) {
      if (cache[num] == undefined) {
        let result= cb(num);
  
        cache[num] = result;
  
        return result;
      } else {
        return cache[num];
      }
    }
  
    return {execute}
 } catch (error) {
    console.log(`${error}`)
 }
}

module.exports = cacheFunction;
