function limitFunctionCallCount(cb, n) {
   try {
    let count = 0;

   return function() {
        if (count < n) {
            count++;
            return cb();
        }
        return null; 
    };
    
   } catch (error) {
    console.log(`${error}`)
   }
}

module.exports = limitFunctionCallCount;