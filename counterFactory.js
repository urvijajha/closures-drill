function counterFactory() {
    try {
        let count = 0;

    function increment() {
        count++;
        return count;
    }

    function decrement() {
        count--;
        return count;
    }

    return {
        increment: increment,
        decrement: decrement
    };
    } catch (error) {
        console.log(`${error}`)
    }
}

module.exports = counterFactory;